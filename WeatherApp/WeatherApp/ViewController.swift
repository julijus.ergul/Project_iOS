//
//  ViewController.swift
//  WeatherApp
//
//  Created by Admin on 2018-07-31.
//  Copyright © 2018 Julijus Ergül. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var apiID = "9dcdb0cf33c76f2cb43a361f338834e9"
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblTemp: UILabel!
    @IBOutlet weak var background: UIImageView!
    @IBOutlet weak var weatherIconImageView: UIImageView!
    var localIdentifier:String?
    @IBAction func changeCity(_ sender: UIBarButtonItem) {
        alertDialog()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        displayBackground()
        
        //download data
        getCityWeatherData("Stockholm"){ (cityName, weather, desc, icon, lat, lng, countryCode) in
            //code
            self.lblCity.text = cityName
            self.lblTemp.text = String(Int(weather)) + "°C"
            self.lblDesc.text = desc
            self.downloadWeatherIcon(iconID: icon, completion: { (data) in
                self.weatherIconImageView.image = UIImage(data: data)
            })
            self.getTimeZoneID(cityName: cityName, countryCode: countryCode, completion: {(timeZoneId, date)in
                self.lblCity.text = cityName
                self.lblDate.text = self.dateFromLocation(identifier: timeZoneId)
                self.lblTime.text = self.timeFromLocation(identifier: timeZoneId)
               // print("timeZone: \(cityName) : \(timeZoneId)")
            })
        
        }
    }//viewDidLoad
    
    //Random background generated
    func displayBackground(){
        background.image = UIImage(named:"gardient1.jpg")
    }//displayBackground
    
    func alertDialog(){
        let alert = UIAlertController(title: "Select City", message: "", preferredStyle: .alert)
        //var changeCity:ChangeCity
        var cityField:UITextField?
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: {(action) in
            cityField = alert.textFields?[0]
            self.changeCity(city: cityField!.text!)
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {(action)in
            //empty
        })
        alert.addTextField{(cityField) in cityField.placeholder = "City name..."}
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    
  func changeCity(city:String){
        //print("city name is \(city)")
        getCityWeatherData(city){ (cityName, weather, desc, icon, lat, lng, countryCode)  in
            //code
            self.lblCity.text = cityName
            self.lblTemp.text = String(Int(weather)) + "°C"
            self.lblDesc.text = desc
            self.downloadWeatherIcon(iconID: icon, completion: { (data) in
                self.weatherIconImageView.image = UIImage(data: data)
            })
            self.getTimeZoneID(cityName: cityName, countryCode: countryCode, completion: {(timeZoneId, date)in
                self.lblCity.text = cityName
                self.localIdentifier = timeZoneId
                self.lblDate.text = self.dateFromLocation(identifier: timeZoneId)
                self.lblTime.text = self.timeFromLocation(identifier: timeZoneId)
                //print("timeZone: \(cityName) : \(timeZoneId)")
                
            })
        }
    }
    func dateFromLocation(identifier:String)->String{
        var dateStr:String?
        let currentDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat="EEE d MMMM yyyy"
        dateFormatter.timeZone = TimeZone(identifier: identifier)
        dateStr = dateFormatter.string(from: currentDate)
        return dateStr!
    }
    func timeFromLocation(identifier:String)->String{
        var timeStr:String?
        
        let currentDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat="HH:mm"
        dateFormatter.timeZone = TimeZone(identifier: identifier)
        timeStr = dateFormatter.string(from: currentDate)
        
        return timeStr!
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }//didReceiveMemoryWarning


}//ViewController
