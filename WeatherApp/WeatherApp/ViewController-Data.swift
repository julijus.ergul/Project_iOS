//
//  ViewController-Data.swift
//  WeatherApp
//
//  Created by Admin on 2018-08-28.
//  Copyright © 2018 Julijus Ergül. All rights reserved.
//

import UIKit

extension ViewController{
    func getCityWeatherData(_ stringCity:String, completion:@escaping(_ cityName:String, _ weather:Double,_ desc:String, _ icon:String, _ lat:Double,_ lng:Double , _ countryCode:String) ->()){
        
        let cityFiltered:String = stringCity.replacingOccurrences(of: " ", with: "+")
        let cityFiltered1:String = cityFiltered.replacingOccurrences(of: "ö", with: "o")
        let cityFiltered2:String = cityFiltered1.replacingOccurrences(of: "ä", with: "a")
        let cityFiltered3:String = cityFiltered2.replacingOccurrences(of: "å", with: "a")
        
        let url:URL = URL(string:"https://api.openweathermap.org/data/2.5/weather?q=\(cityFiltered3)&appid=\(apiID)")!
        
        let task = URLSession.shared.dataTask(with: url){ (data, response, error) in
            if error == nil {
                if let dataValid = data{
                    do{
                        let jsonDict = try JSONSerialization.jsonObject(with: dataValid, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        print(jsonDict)
                        
                        let coord = jsonDict["coord"] as! NSDictionary
                        let latCoord = coord["lat"] as! Double
                        let lonCoord = coord["lon"] as! Double
                        
                        
                        let weatherArray = jsonDict["weather"] as! NSArray
                        let weather = weatherArray[0] as! NSDictionary
                        let description = weather["description"] as! String
                        let iconImage = weather["icon"] as! String
                        
                        self.displayBackground()
                        
                        let sys = jsonDict["sys"] as! NSDictionary
                        let countryCode = sys["country"] as! String
                        
                        let main = jsonDict["main"] as! NSDictionary
                        let temp = main["temp"] as! Double
                        let tempCelsius:Double
                        tempCelsius = temp - 272.15
                        
                        let name = jsonDict["name"] as! String
                        
                        
                        
                        DispatchQueue.main.async(execute: {
                            completion(name,tempCelsius,description,iconImage,latCoord,lonCoord,countryCode)
                        })
                        
                    }catch{
                        print(error.localizedDescription)
                    }//do
                }//if-let
            }//if
        }//task
        task.resume()
        
    }//getCityWeatherData
    func downloadWeatherIcon(iconID:String, completion:@escaping (_ imgData:Data)->()){
        
        let url:URL = URL(string: "http://openweathermap.org/img/w/\(iconID).png")!
        let task = URLSession.shared.dataTask(with: url){
            (data, response, error) in
            if error == nil{
                if let dataOK = data{
                    do{
                        DispatchQueue.main.async(execute: {
                            completion(dataOK)
                        })
                    }catch{
                        print(error.localizedDescription)
                    }//do
                }//if-let
            }//if
        }//task
        task.resume()
    }//downloadWeatherIcon
    
    func getTimeZoneID(cityName:String, countryCode:String, completion:@escaping (_ identifier:String, _ date:String)->()){
        //let apiKey = "04L3ANOZ9GND"
        
        let cityFiltered:String = cityName.replacingOccurrences(of: " ", with: "+")
        let cityFiltered1:String = cityFiltered.replacingOccurrences(of: "ö", with: "o")
        let cityFiltered2:String = cityFiltered1.replacingOccurrences(of: "ä", with: "a")
        let cityNameFiltered:String = cityFiltered2.replacingOccurrences(of: "å", with: "a")
    
        let url = URL(string: "http://vip.timezonedb.com/v2/get-time-zone?key=04L3ANOZ9GND&format=json&by=city&city=\(cityNameFiltered)&country=\(countryCode)")
        var identifier:String?
        
        
        let task = URLSession.shared.dataTask(with: url!){ (data, response, error) in
            if error == nil {
                if let dataValid = data{
                    do{
                        let jsonDict = try JSONSerialization.jsonObject(with: dataValid, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        
                        print("timezoneDict: \(jsonDict)")
                        let zones = jsonDict["zones"] as! NSArray
                        let zoneName = zones[0] as! NSDictionary
                        identifier = zoneName["zoneName"] as! String
                        let date = zoneName["formatted"] as! String
                        print("zoneName: \(identifier!)")
                        
                        DispatchQueue.main.async(execute: {
                            completion(identifier!, date)
                        })
                        
                    }catch{
                        print(error.localizedDescription)
                    }//do
                }//if-let
            }//if
        }//task
        task.resume()
    }//getTimeZoneID
}
