//
//  ChangeCity.swift
//  WeatherApp
//
//  Created by Admin on 2018-08-28.
//  Copyright © 2018 Julijus Ergül. All rights reserved.
//

import UIKit


class ChangeCity: UIViewController {
    
    var cityWeather:ViewController?
    func changeCity(city:String){
        
        print("city name is \(city)")
        cityWeather?.getCityWeatherData(city){ (cityName, weather, desc, icon, lat, lng, countryCode)  in
            //code
            self.cityWeather?.lblCity.text = cityName
            self.cityWeather?.lblTemp.text = String(Int(weather)) + "°C"
            self.cityWeather?.lblDesc.text = desc
            self.cityWeather?.downloadWeatherIcon(iconID: icon, completion: { (data) in
                self.cityWeather?.weatherIconImageView.image = UIImage(data: data)
            })
            self.cityWeather?.getTimeZoneID(cityName: cityName, countryCode: countryCode, completion: {(timeZoneId, date)in
                self.cityWeather?.lblCity.text = cityName
                self.cityWeather?.lblDate.text = self.cityWeather?.dateFromLocation(identifier: timeZoneId)
                self.cityWeather?.lblTime.text = self.cityWeather?.timeFromLocation(identifier: timeZoneId)
                print("timeZone: \(cityName) : \(timeZoneId)")
                
            })
        }
    }

}
